# Generated by Django 2.2.17 on 2020-11-23 12:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pinaxcon_registrasion', '0013_auto_20201008_2036'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attendeeprofile',
            name='address_line_1',
            field=models.CharField(blank=True, help_text='This address, if provided, will appear on your invoices. It is also where we will ship your Swag Badge if you are allocated one.', max_length=1024, verbose_name='Address line 1'),
        ),
        migrations.AlterField(
            model_name='attendeeprofile',
            name='future_conference',
            field=models.BooleanField(blank=True, default=False, help_text='Select to have your login details made available to future Linux Australia conferences who share the same Single Sign On system.', verbose_name='Reuse my login for future Linux Australia conferences?'),
        ),
        migrations.AlterField(
            model_name='attendeeprofile',
            name='lca_announce',
            field=models.BooleanField(blank=True, help_text='Select to be subscribed to the low-traffic lca-announce mailing list', verbose_name='Subscribe to lca-announce list'),
        ),
        migrations.AlterField(
            model_name='attendeeprofile',
            name='lca_chat',
            field=models.BooleanField(blank=True, help_text='lca-chat is a high-traffic mailing list used by attendees during the week of the conference for general discussion.', verbose_name='Subscribe to the LCA chat list'),
        ),
        migrations.AlterField(
            model_name='attendeeprofile',
            name='linux_australia',
            field=models.BooleanField(blank=True, help_text="Select this field to register for free <a href='http://www.linux.org.au/'>Linux Australia</a> membership.", verbose_name='Linux Australia membership'),
        ),
        migrations.AlterField(
            model_name='attendeeprofile',
            name='of_legal_age',
            field=models.BooleanField(blank=True, default=False, help_text='Being under 18 will not stop you from attending the conference. We need to know whether you are over 18 to allow us to cater for you at venues that serve alcohol.', verbose_name='Are you over 18?'),
        ),
    ]
