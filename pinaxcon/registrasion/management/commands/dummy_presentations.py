from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from symposion.conference.models import Section, current_conference

from symposion.speakers.models import Speaker
from symposion.schedule.models import Presentation
from symposion.proposals.models import ProposalKind
from pinaxcon.proposals.models import TalkProposal

User = get_user_model()


class Command(BaseCommand):

    help = "Creates a bunch of dummy presentations to play around with."

    def handle(self, *args, **options):
        conf = current_conference()
        section = Section.objects.filter(conference=conf, slug="main").all().first()

        user = User.objects.first()
        speaker = Speaker.objects.first()
        if not speaker:
            speaker, _ = Speaker.objects.get_or_create(name="Dummy Speaker",
                                                       defaults={"user": user})
        talk_kind = ProposalKind.objects.first()
        target_audience = TalkProposal.TARGET_USER

        for i in range(1000, 1020):
            prop, _created = TalkProposal.objects.get_or_create(
                pk=i, kind=talk_kind, speaker=speaker, target_audience=target_audience,
                title=f"dummy title {i}", abstract=f"dummy abstract {i}")

            pres, _created = Presentation.objects.get_or_create(
                proposal_base=prop, section=section, speaker=speaker,
                title=f"dummy title {i}", abstract=f"dummy abstract {i}")
