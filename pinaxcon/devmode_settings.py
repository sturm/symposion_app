EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
AUTHENTICATION_BACKENDS = [
    'symposion.teams.backends.TeamPermissionsBackend',
    'django.contrib.auth.backends.ModelBackend',
]
LOGIN_URL='/accounts/login'

ROOT_URLCONF = "pinaxcon.devmode_urls"

DEFAULT_FILE_STORAGE = 'django.core.files.storage.FileSystemStorage'

INTERNAL_IPS = ['*']
