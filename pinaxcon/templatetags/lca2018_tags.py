import hashlib

from decimal import Decimal
from django import template
from django.conf import settings
from django.contrib.staticfiles.templatetags import staticfiles
from easy_thumbnails.files import get_thumbnailer
from registrasion.templatetags import registrasion_tags
from symposion.conference import models as conference_models
from symposion.schedule.models import Track
from regidesk.models import CheckIn

CONFERENCE_ID = settings.CONFERENCE_ID
GST_RATE = settings.GST_RATE

register = template.Library()


@register.simple_tag()
def classname(ob):
    return ob.__class__.__name__


@register.simple_tag(takes_context=True)
def can_manage(context, proposal):
    return proposal_permission(context, "manage", proposal)


@register.simple_tag(takes_context=True)
def can_review(context, proposal):
    return proposal_permission(context, "review", proposal)


def proposal_permission(context, permname, proposal):
    slug = proposal.kind.section.slug
    perm = "reviews.can_%s_%s" % (permname, slug)
    return context.request.user.has_perm(perm)


@register.simple_tag(takes_context=True)
def speaker_photo(context, speaker, size):
    ''' Provides the speaker profile, or else fall back to libravatar or gravatar. '''

    if speaker.photo:
        thumbnailer = get_thumbnailer(speaker.photo)
        thumbnail_options = {'crop': True, 'size': (size, size)}
        thumbnail = thumbnailer.get_thumbnail(thumbnail_options)
        return thumbnail.url
    else:
        email = speaker.user.email.encode("utf-8")
        md5sum = hashlib.md5(email.strip().lower()).hexdigest()
        fallback_image = ("https://linux.conf.au/site_media/static/lca2017"
                          "/images/speaker-fallback-devil.jpg")
        url = "https://secure.gravatar.com/avatar/%s?s=%d&d=%s" % (md5sum, size, "mp")

        return url


@register.simple_tag()
def define(value):
    return value


@register.simple_tag()
def presentation_bg_number(presentation, count):
    return sum(ord(i) for i in presentation.title) % count


@register.filter()
def gst(amount):
    value_no_gst = Decimal(amount / (1 + GST_RATE))
    return Decimal(amount - value_no_gst).quantize(Decimal('0.01'))


@register.simple_tag()
def conference_name():
    return conference_models.Conference.objects.get(id=CONFERENCE_ID).title


@register.simple_tag()
def conference_start_date():
    return conference_models.Conference.objects.get(id=CONFERENCE_ID).start_date


@register.simple_tag()
def conference_end_date():
    return conference_models.Conference.objects.get(id=CONFERENCE_ID).end_date


@register.simple_tag()
def conference_timezone():
    return conference_models.Conference.objects.get(id=CONFERENCE_ID).timezone


@register.filter()
def day_has_tracks(timetable, day):
    try:
        track_names = day.track_set.all()
        has_tracks = True
    except Track.DoesNotExist:
        has_tracks = False
    return len(track_names)


@register.filter()
def trackname(room, day):
    if not room:
        return None

    try:
        track_name = room.track_set.get(day=day).name
    except Track.DoesNotExist:
        track_name = None
    return track_name


@register.simple_tag(takes_context=True)
def ticket_type(context):

    # Default to purchased ticket type (only item from category 1)
    items = registrasion_tags.items_purchased(context, 1)

    if not items:
        return "NO TICKET"

    item = next(iter(items))
    name = item.product.name
    if name == "Conference Volunteer":
        return "Volunteer"
    elif name == "Conference Organiser":
        return "Organiser"
    else:
        ticket_type = name


    # Miniconfs are section 2
    # General sessions are section 1

    user = registrasion_tags.user_for_context(context)

    if hasattr(user, "speaker_profile"):
        best = 0
        for presentation in user.speaker_profile.presentations.all():
            if presentation.section.id == 1:
                best = 1
            if best == 0 and presentation.section.id == 2:
                best = 2
        if best == 1:
            return "Speaker"
        elif best == 2:
            return "Miniconf Org"

    if name == "Sponsor":
        return "Professional"
    elif name == "Fairy Penguin Sponsor":
        return "Professional"
    elif name == "Monday and Tuesday Only":
        return "Mon/Tue Only"

    # Default to product type
    return ticket_type


@register.simple_tag(takes_context=True)
def venueless_login_url(context):
    # Check that they have a ticket first (only item from category 1)
    items = registrasion_tags.items_purchased(context, 1)
    if not items:
        return ''

    # Get token from checkin
    user=context.request.user
    checkin = CheckIn.objects.get_or_create(user=user)[0]
    if checkin.venueless_token:
        return f'{settings.VENUELESS_URL}/#token={checkin.venueless_token}'
    return ''
