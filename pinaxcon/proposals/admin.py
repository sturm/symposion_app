from django.contrib import admin

from pinaxcon.proposals import models
from symposion.proposals import models as symposion_models
from symposion.reviews.models import ProposalResult

class CategoryAdmin(admin.ModelAdmin):

    class AdditionalSpeakerInline(admin.TabularInline):
        model = symposion_models.AdditionalSpeaker

    class ProposalResultInline(admin.TabularInline):
        model = ProposalResult
        readonly_fields = ["score"]
        fields = ["status"]

    inlines = [
        AdditionalSpeakerInline,
        ProposalResultInline,
    ]


models_to_register = [
    models.TalkProposal,
    models.TutorialProposal,
]

for model in models_to_register:
    admin.site.register(model, CategoryAdmin,
                        list_display = [
                            "id",
                            "title",
                            "speaker",
                            "speaker_email",
                            "kind",
                            "target_audience",
                            "status",
                            "cancelled",
                        ],
                        list_filter = [
                            "result__status",
                            "cancelled",
                        ],
    )

