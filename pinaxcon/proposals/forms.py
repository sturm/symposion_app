import copy

from django import forms

from pinaxcon.proposals.fields import HelpTextField
from pinaxcon.proposals import models


DEFAULT_FIELDS =  [
    "title",
    "primary_topic",
    "target_audience",
    "experience_level",
    "abstract",
    "private_abstract",
    "content_warning",
    "technical_requirements",
    "project",
    "project_url",
    "video_url",
    "require_approval",
    "recording_release",
    "materials_release",
]

class ProposalForm(forms.ModelForm):

    required_css_class = 'label-required'

    def clean_description(self):
        value = self.cleaned_data["description"]
        if len(value) > 400:
            raise forms.ValidationError(
                u"The description must be less than 400 characters"
            )
        return value


class TalkProposalForm(ProposalForm):

    class Meta:
        model = models.TalkProposal
        fields = copy.copy(DEFAULT_FIELDS)


class TutorialProposalForm(ProposalForm):

    class Meta:
        model = models.TutorialProposal
        fields = copy.copy(DEFAULT_FIELDS)
