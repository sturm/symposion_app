from django import forms


class HelpTextWidget(forms.Widget):
    template_name = "forms/widgets/help_widget.html"

    def __init__(self, *args, text=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.text = text

    def render(self, name, value, attrs=None, renderer=None):
        context = {'text': self.text}
        return self._render(self.template_name, context, renderer)


class HelpTextField(forms.Field):
    def __init__(self, *args, text=None, **kwargs):
        super().__init__(self, *args, **kwargs)
        self.required = False

        widget = HelpTextWidget(text=text)
        widget.is_required = False
        self.widget = widget

    def clean(self, value):
        return value