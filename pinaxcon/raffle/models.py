from django.conf import settings
from django.db import models

from pinaxcon.raffle.mixins import PrizeMixin, RaffleMixin


class Raffle(RaffleMixin, models.Model):
    """
    Stores a single Raffle object, related to one or many
    :model:`pinaxcon_registrasion.Product`, which  is usually a raffle ticket,
    but can be set to tickets or other products for door prizes.
    """
    description = models.CharField(max_length=255)
    products = models.ManyToManyField('registrasion.Product')
    hidden = models.BooleanField(default=True)

    def __str__(self):
        return self.description


class Prize(PrizeMixin, models.Model):
    """
    Stores a Prize for a given :model:`pinaxcon_raffle.Raffle`.

    Once `winning_ticket` has been set to a :model:`pinaxcon_raffle.DrawnTicket`
    object, no further changes are permitted unless the object is explicitely
    unlocked.
    """
    description = models.CharField(max_length=255)
    raffle = models.ForeignKey(
        'pinaxcon_raffle.Raffle',
        related_name='prizes',
        on_delete=models.CASCADE,
    )
    order = models.PositiveIntegerField()
    winning_ticket = models.OneToOneField(
        'pinaxcon_raffle.DrawnTicket',
        null=True,
        blank=True,
        related_name='+',
        on_delete=models.PROTECT,
    )

    class Meta:
        unique_together = ('raffle', 'order')

    def __str__(self):
        return f"{self.order}. Prize: {self.description}"


class PrizeAudit(models.Model):
    """
    Stores an audit event for changes to a particular :model:`pinaxcon_raffle.Prize`.
    """
    reason = models.CharField(max_length=255)
    prize = models.ForeignKey(
        'pinaxcon_raffle.Prize',
        related_name='audit_events',
        on_delete=models.CASCADE,
    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-timestamp',)

    def __str__(self):
        return self.reason


class Draw(models.Model):
    """
    Stores a draw for a given :model:`pinaxcon_raffle.Raffle`, along with audit fields
    for the creating :model:`auth.User` and the creation timestamp.
    """
    raffle = models.ForeignKey(
        'pinaxcon_raffle.Raffle',
        related_name='draws',
        on_delete=models.CASCADE,
    )
    drawn_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    drawn_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.raffle}: {self.drawn_time}"


class DrawnTicket(models.Model):
    """
    Stores the result of a ticket draw, along with the corresponding
    :model:`pinaxcon_raffle.Draw`, :model:`pinaxcon_raffle.Prize` and the
    :model:`registrasion.commerce.LineItem` from which it was generated.
    """
    ticket = models.CharField(max_length=255)

    draw = models.ForeignKey(
        'pinaxcon_raffle.Draw',
        on_delete=models.CASCADE,
    )
    prize = models.ForeignKey(
        'pinaxcon_raffle.Prize',
        on_delete=models.CASCADE,
    )
    lineitem = models.ForeignKey(
        'registrasion.LineItem',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"{self.ticket}: {self.draw.raffle}"
