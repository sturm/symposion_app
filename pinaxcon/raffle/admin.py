from django.contrib import admin

from pinaxcon.raffle import models


class ReadOnlyMixin:
    actions = None
    list_display_links = None

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def save_model(self, request, obj, form, change):
        return


class DrawAdmin(ReadOnlyMixin, admin.ModelAdmin):
    list_display = ('raffle', 'drawn_time', 'drawn_by')
    readonly_fields = ('raffle', 'drawn_time', 'drawn_by')
    list_filter = ('raffle',)

    ordering = ('raffle', '-drawn_time')


class DrawnTicketAdmin(ReadOnlyMixin, admin.ModelAdmin):
    list_display = ('draw', 'ticket')
    readonly_fields = ('draw', 'ticket', 'lineitem', 'prize')


class AuditAdmin(ReadOnlyMixin, admin.ModelAdmin):
    list_display = ('timestamp', 'raffle', 'prize', 'reason', 'user',)
    list_filter = ('prize__raffle',)
    readonly_fields = ('reason', 'prize', 'user')

    def raffle(self, instance):
        return instance.prize.raffle


class PrizeAdmin(admin.ModelAdmin):
    readonly_fields = ('winning_ticket',)


admin.site.register(models.Raffle)
admin.site.register(models.Prize, PrizeAdmin)
admin.site.register(models.Draw, DrawAdmin)
admin.site.register(models.DrawnTicket, DrawnTicketAdmin)
admin.site.register(models.PrizeAudit, AuditAdmin)
