function (slug, sha) {

  local app = "symposion-app-" + slug,
  local namespace = "rego-review",
  local domain = slug + ".dev.lca2019.org",
  local tls_secret = slug + "-tls",

  "deployment.json":
    {
      "apiVersion": "v1",
      "items": [
        {
          "apiVersion": "v1",
          "kind": "Service",
          "metadata": {
            "labels": {
              "app":  app,
              "slug": slug
            },
            "name": app,
            "namespace": namespace
          },
          "spec": {
            "ports": [
              {
                "port": 8000,
                "protocol": "TCP",
                "targetPort": 8000
              }
            ],
            "selector": {
              "app": app,
              "slug": slug
            },
            "sessionAffinity": "None",
            "type": "ClusterIP"
          }
        },
        {
          "apiVersion": "extensions/v1beta1",
          "kind": "Deployment",
          "metadata": {
            "labels": {
              "app": app,
              "slug": slug,
            },
            "name": app,
            "namespace": namespace
          },
          "spec": {
            "replicas": 1,
            "revisionHistoryLimit": 1,
            "selector": {
              "matchLabels": {
                "app": app,
                "slug": slug
              }
            },
            "strategy": {
              "rollingUpdate": {
                "maxSurge": 1,
                "maxUnavailable": 1
              },
              "type": "RollingUpdate"
            },
            "template": {
              "metadata": {
                "labels": {
                  "app": app,
                  "slug": slug
                }
              },
              "spec": {
                "containers": [
                  {
                    "env": [
                      {
                        "name": "SYMPOSION_APP_DEBUG",
                        "value": "1"
                      },
                      {
                        "name": "DJANGO_SECRET_KEY",
                        "value": "paGhahQuain5ohYoh0moorai"
                      },
                      {
                        "name": "DATABASE_URL",
                        "value": "sqlite:////tmp/symposion.sqlite"
                      },
                      {
                        "name": "GCS_BUCKET",
                        "value": "CEA51A5-A613-4AEF-A9FB-D0A57D77C13B"
                      },
                      {
                        "name": "GOOGLE_APPLICATION_CREDENTIALS",
                        "value": "/dev/null"
                      },
                      {
                        "name": "STRIPE_PUBLIC_KEY",
                        "valueFrom": {
                          "secretKeyRef": {
                            "key": "STRIPE_PUBLIC_KEY",
                            "name": "symposion-app-config"
                          }
                        }
                      },
                      {
                        "name": "STRIPE_SECRET_KEY",
                        "valueFrom": {
                          "secretKeyRef": {
                            "key": "STRIPE_SECRET_KEY",
                            "name": "symposion-app-config"
                          }
                        }
                      },
                      {
                        "name": "SYMPOSION_DEV_MODE",
                        "value": "LAPTOP"
                      },
                      {
                        "name": "ANALYTICS_KEY",
                        "value": "UA-000000000-1"
                      }
                    ],
                    "image": "registry.gitlab.com/laconfdev/symposion_app/2023:" + sha,
                    "imagePullPolicy": "Always",
                    "livenessProbe": {
                      "failureThreshold": 3,
                      "httpGet": {
                        "path": "/admin/login/",
                        "port": 8000,
                        "scheme": "HTTP"
                      },
                      "initialDelaySeconds": 180,
                      "periodSeconds": 10,
                      "successThreshold": 1,
                      "timeoutSeconds": 5
                    },
                    "name": app,
                    "ports": [
                      {
                        "containerPort": 8000,
                        "protocol": "TCP"
                      }
                    ],
                    "resources": {},
                    "terminationMessagePath": "/dev/termination-log",
                    "terminationMessagePolicy": "File"
                  }
                ],
                "dnsPolicy": "ClusterFirst",
                "restartPolicy": "Always",
                "schedulerName": "default-scheduler",
                "securityContext": {},
                "terminationGracePeriodSeconds": 30
              }
            }
          }
        }
      ],
      "kind": "List"
    },
  "ingress.json":
    {
        "kind": "Ingress",
        "apiVersion": "extensions/v1beta1",
        "metadata": {
            "name": app,
            "namespace": namespace,
            "annotations": {
                "kubernetes.io/ingress.class": "nginx-review",
            }
        },
        "spec": {
            "backend": {
                "serviceName": app,
                "servicePort": 80
            },
            "tls": [
                {
                    "hosts": [
                        domain
                    ],
                    "secretName": tls_secret,
                }
            ],
            "rules": [
                {
                    "host": domain,
                    "http": {
                        "paths": [
                            {
                                "path": "/",
                                "backend": {
                                    "serviceName": app,
                                    "servicePort": 8000
                                }
                            }
                        ]
                    }
                }
            ]
        }
    },
  "certificate.json":
    {
      "apiVersion": "certmanager.k8s.io/v1alpha1",
      "kind": "Certificate",
      "metadata": {
        "name": slug,
        "namespace": "rego-review",
      },
      "spec": {
        "acme": {
          "config": [
            {
              "domains": [
                domain
                    ],
              "http01": {
                      "ingressClass": "nginx-review"
                    }
                }
            ]
        },
        "commonName": "",
        "dnsNames": [
            domain
        ],
        "issuerRef": {
            "kind": "ClusterIssuer",
            "name": "letsencrypt-prod"
        },
        "secretName": tls_secret
      }
    }
}
