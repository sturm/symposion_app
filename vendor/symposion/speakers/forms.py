from django import forms

from symposion.speakers.models import Speaker


class SpeakerForm(forms.ModelForm):

    required_css_class = 'label-required'

    class Meta:
        model = Speaker
        fields = [
            "name",
            "pronouns",
            "biography",
            "experience",
            "photo",
            "telephone",
            "local_timezone",
            "homepage",
            "twitter_username",
            "mastodon_username",
            "accessibility",
            "travel_assistance",
            "accommodation_assistance",
            "assistance",
            "agreement",
        ]

    def __init__(self, *a, **k):
        super(SpeakerForm, self).__init__(*a, **k)
        self.fields['agreement'].required = True
        self.fields['biography'].required = True
        self.fields['local_timezone'].required = True

    def clean_twitter_username(self):
        value = self.cleaned_data["twitter_username"]
        if value.startswith("@"):
            value = value[1:]
        return value
